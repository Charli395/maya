<?php
session_start(); 
?>
<!DOCTYPE html>
<html>
	<head>
    <title>Capacitación lv1</title>
    
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../estilos.css">
    <link rel="stylesheet" href="../fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Montez|Pathway+Gothic+One" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <style>
.btn {
  background-color: red;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;

}

/* Darker background on mouse-over */
.btn:hover {
  background-color: #ee3a1f;
}
#myProgress {
  position: relative;
  width: 100%;
  height: 30px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 10%;
  height: 100%;
  background-color: red;
}

#label {
  text-align: center;
  line-height: 30px;
  color: white;
}
</style>
    <!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<link rel="stylesheet" href="css/custom.css">
	<!--Styel para el fondo del mapa minero-->
  </head>
  <br>
  <header class="contenedor">	
  <div class="container">
	<div class="row">	
		<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="row">
			<div class="form-group">				
				<img src="../img/user.png" alt="User" width="90px" height="90px">
		  </div>
		  <div class="form-group">				
			  <?php
                if (isset($_SESSION['nombre'],$_SESSION['puesto'])) {

                  echo "
                  <br>
                    <div><strong><p style='color:black' align='center'>$_SESSION[nombre]</p></strong>
                    <p style='color:black' align='top' >$_SESSION[puesto]/$_SESSION[unidad]/$_SESSION[contratista]</p>
					</div>
                   

                  ";
                }else{
                    echo '<script type="text/javascript">location.href="../index.html"</script>';
                }
              ?>
		  </div>
		</div><!-- termina primer row del row-->	  
		  
		</div><!-- termina primer columna-->
			
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">				
				<img src="../img/fresnillo.png" align="right">
		  </div>
		</div>
	</div>
</div>
</header>

<!-- inicia el body con el mapa interactivo-->
<body class="contenedor">
  <div class="container">
    <div class="progress">
      <div class="progress-bar progress-bar-success" role="progressbar" style="width:0%">
        12.5%
      </div>
    </div>
</div>

</body>  
<footer align="center" class="contenedor">
      <p> © 2019 Maya producciones. Todos los derechos reservados</p>
  </footer>
<!-- Termina el body-->
</html>              
