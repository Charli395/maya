 <?php include "database.php"; ?>
<?php
session_start(); 
?>
<?php
	//Get the total questions
	$query="select * from questions3";
	//Get Results
	$results = $mysqli->query($query) or die ($mysqli->error.__LINE__);
	$total = $results->num_rows;

 ?>
<!DOCTYPE html>
<html>
  <head>
    <title></title>
   <meta charset="utf-8">
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../estilos.css">
    <link rel="stylesheet" href="../fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Montez|Pathway+Gothic+One" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <style>
.btn {
  background-color: red;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;

}

/* Darker background on mouse-over */
.btn:hover {
  background-color: #ee3a1f;
}
#myProgress {
  position: relative;
  width: 100%;
  height: 30px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 10%;
  height: 100%;
  background-color: red;
}

#label {
  text-align: center;
  line-height: 30px;
  color: white;
}
</style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css">
  <!--Styel para el fondo del mapa minero-->
  </head>
  <br>
  <header class="contenedor"> 
  <div class="container">
  <div class="row"> 
    <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="row">
      <div class="form-group">        
        <img src="../img/user.png" alt="User" width="90px" height="90px">
      </div>
      <div class="form-group">        
        <?php
                if (isset($_SESSION['nombre'],$_SESSION['puesto'])) {

                  echo "
                  <br>
                    <div><strong><p style='color:black' align='center'>$_SESSION[nombre]</p></strong>
                    <p style='color:black' align='top' >$_SESSION[puesto]/$_SESSION[unidad]/$_SESSION[contratista]</p>
          </div>
                   

                  ";
                }else{
                    echo '<script type="text/javascript">location.href="../index.html"</script>';
                }
              ?>
      </div>
    </div><!-- termina primer row del row-->    
      
    </div><!-- termina primer columna-->
      
    <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">        
        <img src="../img/fresnillo.png" align="right">
      </div>
    </div>
  </div>
</div>
</header>

  <body class="contenedor">
    <body class="contenedor">
    <div class="container">
   
      <div class="form-group" align="center">        
        <img src="../img/barrenacion.png" width="60px" height="60px">
        <strong><p>Barrenacion</p></strong>
      </div>
    
    </div>
    <div>
      <video id="demo" width="650" height="350"src="../videos/barrenacion.mp4" type="video/mp4">
      </video>

      <div align="center">
       <button  class="btn" onclick="document.getElementById('demo').play()"><i class="fa fa-play-circle"></i>Play</button>
       <button  class="btn" onclick="document.getElementById('demo').pause()"><i class="fa fa-pause-circle"></i>Pause</button>
    </div>
    </div> <!--play y pause del video -->
    <br>
    <script type="text/javascript">
      var vid=document.getElementById("demo");
      function myFunction() {
        var completo=vid.ended;
        if (completo==false) {
          var mensaje="¡ups, no puedes continuar hasta terminar de ver el video!";
          alert(mensaje);
          vid.autoplay = true;
          vid.load();
        }else{
          location.replace('question.php?n=1');
        }
      }// termina funcion

    </script>
    
    <div>
      <button id="comenzar" class="btn btn-danger btn-block" onclick="myFunction()">Comenzar</button>
        <a href="question.php?n=1">link</a>
    </div>
    <!-- diva para controles del video-->
    <br>
    <br>
</body> 
	<!--<a href="question.php?n=1" class="start">Start Quiz</a>-->
    
  </body>
  <footer align="center" class="contenedor">
      <p> © 2019 Maya producciones. Todos los derechos reservados</p>
  </footer>
</html>