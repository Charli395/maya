<?php
session_start(); 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Mapa</title>
    
  <!-- Required meta tags -->
  <script type="text/javascript">
  
  function nobackbutton(){
  
   window.location.hash="no-back-button";
  
   window.location.hash="Again-No-back-button" //chrome
  
   window.onhashchange=function(){window.location.hash="no-back-button";}
  
}
</script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Crear Sesión</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Montez|Pathway+Gothic+One" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css">
  <!--Styel para el fondo del mapa minero-->
    <style type="text/css">

      body {
        background: rgba(153,153,153, 0.25);
        overflow-x: hidden;
      }

/*imagenes*/

        #fondo {
          position: absolute;
            width: 100%;
            height: auto;
          }

        #mzorro {
          position: absolute;
          width: 25.3%;
            height: auto;
            }

        #vagon {
          position: absolute;
          width: 25.3%;
            height: auto;
            }


/*zona interactiva*/

        .js .imagemap__item {
           display: none;
          }
        @media screen and (min-width: 48em) {

          .imagemap__item:target {
             display: block;
            }
          .imagemap__item:target > .imagemap__text {
              display: block;
           }
                          }

/*pulse*/
        .bounce-top {
          -webkit-animation: bounce-top 1s both;
                animation: bounce-top 1s both;
          }


@-webkit-keyframes bounce-top {
  0% {
    -webkit-transform: translateY(-1500px);
            transform: translateY(-1500px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
    opacity: 1;
  }
  24% {
    opacity: 1;
  }
  40% {
    -webkit-transform: translateY(-24px);
            transform: translateY(-24px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  65% {
    -webkit-transform: translateY(-12px);
            transform: translateY(-12px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  82% {
    -webkit-transform: translateY(-6px);
            transform: translateY(-6px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  93% {
    -webkit-transform: translateY(-4px);
            transform: translateY(-4px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  25%,
  55%,
  75%,
  87% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-animation-timing-function: ease-out;
            animation-timing-function: ease-out;
  }
  100% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-animation-timing-function: ease-out;
            animation-timing-function: ease-out;
    opacity: 1;
  }
}
@keyframes bounce-top {
  0% {
    -webkit-transform: translateY(-45px);
            transform: translateY(-45px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
    opacity: 1;
  }
  24% {
    opacity: 1;
  }
  40% {
    -webkit-transform: translateY(-24px);
            transform: translateY(-24px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  65% {
    -webkit-transform: translateY(-12px);
            transform: translateY(-12px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  82% {
    -webkit-transform: translateY(-6px);
            transform: translateY(-6px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  93% {
    -webkit-transform: translateY(-4px);
            transform: translateY(-4px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  25%,
  55%,
  75%,
  87% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-animation-timing-function: ease-out;
            animation-timing-function: ease-out;
  }
  100% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-animation-timing-function: ease-out;
            animation-timing-function: ease-out;
    opacity: 1;
  }
}

    </style>


  </head>
  <br>
  <header class="contenedor"> 
  <div class="container">
  <div class="row"> 
    <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="row">
      <div class="form-group">        
        <img src="../img/user.png" alt="User" width="90px" height="90px">
      </div>
      <div class="form-group">        
        <?php
                if (isset($_SESSION['nombre'],$_SESSION['puesto'])) {

                  echo "
                  <br>
                    <div><strong><p style='color:black' align='center'>$_SESSION[nombre]</p></strong>
                    <p style='color:black' align='top' >$_SESSION[puesto]/$_SESSION[unidad]/$_SESSION[contratista]</p>
          </div>
                   

                  ";
                }else{
                    echo '<script type="text/javascript">location.href="../index.html"</script>';
                }
              ?>
      </div>
    </div><!-- termina primer row del row-->    
      
    </div><!-- termina primer columna-->
      
    <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">        
        <img src="../img/fresnillo.png" align="right">
      </div>
    </div>
  </div>
</div>
</header>

		<style type="text/css">
body {
        background: rgba(153,153,153, 0.25);
        overflow-x: hidden;
      }

/*imagenes*/

				#fondo {
					position: absolute;
  					width: 100%;
  					height: auto;
					}

				#zorro2 {
					position: absolute;
					width: 10.9%;
  					height: auto;
  					margin-left: 550px;

				    }


				button{
					position: absolute;
				}

/*zona interactiva*/

				.js .imagemap__item {
 					 display: none;
					}
				@media screen and (min-width: 48em) {

  				.imagemap__item:target {
   					 display: block;
  					}
 			    .imagemap__item:target > .imagemap__text {
  					  display: block;
 					 }
													}

/*pulse*/
				.bounce-top {
					-webkit-animation: bounce-top 1s both;
	      			  animation: bounce-top 1s both;
					}


@-webkit-keyframes bounce-top {
  0% {
    -webkit-transform: translateY(-1500px);
            transform: translateY(-1500px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
    opacity: 1;
  }
  24% {
    opacity: 1;
  }
  40% {
    -webkit-transform: translateY(-24px);
            transform: translateY(-24px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  65% {
    -webkit-transform: translateY(-12px);
            transform: translateY(-12px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  82% {
    -webkit-transform: translateY(-6px);
            transform: translateY(-6px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  93% {
    -webkit-transform: translateY(-4px);
            transform: translateY(-4px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  25%,
  55%,
  75%,
  87% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-animation-timing-function: ease-out;
            animation-timing-function: ease-out;
  }
  100% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-animation-timing-function: ease-out;
            animation-timing-function: ease-out;
    opacity: 1;
  }
}
@keyframes bounce-top {
  0% {
    -webkit-transform: translateY(-45px);
            transform: translateY(-45px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
    opacity: 1;
  }
  24% {
    opacity: 1;
  }
  40% {
    -webkit-transform: translateY(-24px);
            transform: translateY(-24px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  65% {
    -webkit-transform: translateY(-12px);
            transform: translateY(-12px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  82% {
    -webkit-transform: translateY(-6px);
            transform: translateY(-6px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  93% {
    -webkit-transform: translateY(-4px);
            transform: translateY(-4px);
    -webkit-animation-timing-function: ease-in;
            animation-timing-function: ease-in;
  }
  25%,
  55%,
  75%,
  87% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-animation-timing-function: ease-out;
            animation-timing-function: ease-out;
  }
  100% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-animation-timing-function: ease-out;
            animation-timing-function: ease-out;
    opacity: 1;
  }
}

		</style>

	</head>


	<body onload="nobackbutton();carga();">

	<!-- fondo	 -->			
	<div class="m-imagemap">
		<img id="fondo" class="imagemap__img" src="icono2.png" alt="anclaje" usemap="#imagemap1" >
			<map  id="imagemap1" name="imagemap1">
<!-- <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> -->
 				<area shape="circle" alt="anclaje" coords="2799,848,129"  id ="run"   /> 
  

						<script >  
  							 document.addEventListener("click", functionOne);

 							 function functionOne(){
 							 document.getElementById("run")
  							 }

 
						</script>
			</map>

<!-- imagen zorro y vagon -->
	   
		<img id="zorro2"  src="zorro2.png">


	</div>

<!-- elementos extra -->
<div>
	<area shape="rect" alt="bomba" coords="1469,1229,1763,1403" id="boom" /> 



<script >  
   document.addEventListener("click", functionBom);

  function functionBom(){
  document.getElementById("boom")
  console.log("booom!");
  }

 
</script>


</div>


<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<!-- animacion zorro -->
		<script>
			$( "#run" ).click(function() {
				// recto
  				$( "#zorro2"  ).animate({ "left": "+=200px" }, {duration: 3000, complete:function(){
  						console.log("done");
  						window.location.replace("../3/index.php");
  						},
						});
						});


		</script>


<script type="text/javascript">
	
// mapa responsive
;(function(a)

	{a.fn.rwdImageMaps=function(
		){var c=this;var b=function(
			){c.each(function(
				){if(typeof(a(this).attr("usemap"))=="undefined")
				{return}var e=this,d=a(e);a("<img />").load(function(
					){var g="width",m="height",n=d.attr(g),j=d.attr(m);if(!n||!j)
				     {var o=new Image();o.src=d.attr("src");if(!n){n=o.width}if(!j){j=o.height}}
				      var f=d.width()/100,k=d.height()/100,i=d.attr("usemap").replace("#",""),l="coords";a('map[name="'+i+'"]').find("area").each(function(
				      	){var r=a(this);if(!r.data(l)){r.data(l,r.attr(l))}
				          var q=r.data(l).split(","),p=new Array(q.length);
				          for(var h=0;h<p.length;++h){if(h%2===0){p[h]=parseInt(((q[h]/n)*100)*f)}
				          	else{p[h]=parseInt(((q[h]/j)*100)*k)}}r.attr(l,p.toString())})}).attr("src",d.attr("src"))})};a(window).resize(b).trigger("resize");
							return this}})(jQuery);


$(document).ready(function(e) {
    $('img[usemap]').rwdImageMaps();

});

</script>

</body>





</html>