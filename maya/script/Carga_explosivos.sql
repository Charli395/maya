INSERT INTO `questions5` (`question_number`, `question`) VALUES
(1, '¿Cuáles son los peligros y riesgos?'),
(2, '¿Cuál es el equipo de protección personal?'),
(3, '¿Qué es el barreno?'),
(4, '¿Qués es un bombillo?'),
(5, '¿Qué se debe hacer al llegar al área de trabajo?'),
(6, 'Algunas recomenadaciones para el procedimiento:'),
(7, '¿Qué hacer en caso de que no se detone?'),
(8, '¿De que materiales puede ser el punzón?'),
(9, '¿Qué hacer en caso de que se tenga ,más de una voladura en la misma área?'),
(10, '¿Cuáles son los horarios de detonación según el turno?');

INSERT INTO `choices5` (`id`, `question_number`, `is_correct`, `choice`) VALUES
(30, 1, 1, 'Caída de roca, caída a nivel, sobre esfuerzo, esquirlas en ojos, exposición a ruido, electrocución, exposición a polvo y líquidos presurizados.'),
(31, 1, 0, 'Caída de roca, caída a nivel, sobre esfuerzo'),
(32, 1, 0, 'Ninguno'),
(33, 1, 0, 'Exposición a polvo y líquidos presurizados.'),

(34, 2, 0, 'Cinto con auto rescatador, sordinas, casco.'),
(35, 2, 0, 'Cinto sin auto rescatador, sin sordinas, casco, guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),
(36, 2, 1, 'Cinto con auto rescatador, sordinas, casco, guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),
(37, 2, 0, 'Cinto con guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),

(38, 3, 0, 'Un lugar'),
(39, 3, 0, 'Una herramienta'),
(40, 3, 1, 'Hueco donde se coloca el bombillo'),
(41, 3, 0, 'Un procedimiento'),

(42, 4, 1, 'Explosivo en forma de salchicha'),
(43, 4, 0, 'herramienta'),
(44, 4, 0, 'Procedimiento'),
(45, 4, 0, 'Nada'),

(46, 5, 0, 'Trabajar en cuanto se llegue'),
(47, 5, 0, 'Las 4A'),
(48, 5, 1, 'Verificar la ventilación, el amacice, y el orden y limpieza del sitio'),
(49, 5, 0, 'Nada'),

(50, 6, 0, 'Apagar el motor'),
(51, 6, 0, 'Llenar nichos'),
(52, 6, 1, 'Avisar para desalojar el área, inspeccionar el sitio de trbajo'),
(53, 6, 0, 'Nada'),

(54, 7, 1, 'Avisar por radio, evitar el acceso del personal'),
(55, 7, 0, 'Permitir el acceso del personal '),
(56, 7, 0, 'No avisar'),
(57, 7, 0, 'Detonar nuevamente'),

(58, 8, 0, 'Todas las anteriores'),
(59, 8, 0, 'Acero'),
(60, 8, 0, 'Plástico'),
(61, 8, 1, 'Madera, aluminio'),

(62, 9, 0, 'Trabajar las dos al mismo tiempo'),
(63, 9, 1, 'Se debe iniciar con la que está más lejana a la salida'),
(64, 9, 0, 'Se debe iniciar con la que está cerca de la salida'),
(65, 9, 0, 'No trabajar en ninguna'),

(66, 10, 0, 'Primera:17:00-17:50, Segunda: 5:00,5:20'),
(67, 10, 0, 'Primera:14:00-14:30, Segunda: 7:00,7:30'),
(68, 10, 0, 'Primera:16:00-16:50, Segunda: 3:00,3:40'),
(69, 10, 1, 'Primera:18:00-18:50, Segunda: 6:00,6:50');
