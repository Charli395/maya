INSERT INTO `questions3` (`question_number`, `question`) VALUES
(1, '¿Cuáles son los peligros y riesgos al barrenar?'),
(2, '¿Cuál es el equipo de protección personal?'),
(3, '¿Cuál es el mantenimiento básico para este procedimiento?'),
(4, 'Cuando el equipo va a trabajar por primera vez en un nuevo rebaje, ¿cómo se coloca el simba?'),
(5, '¿Cómo se mueve el simba cuando el equipo ya está en el área de trabajo?'),
(6, 'Al llegar al área de trabajo, ¿qué es lo que se debe revisar?'),
(7, '¿Qué se debe hacer antes de energizar el equipo?'),
(8, '¿Qué hacer en caso de que esté energizada?'),
(9, '¿Cuál es la función de la lona de barricada?'),
(10, '¿Qué se debe hacer cuando el equipo esté cerca de un contrapozo abierto?');

INSERT INTO `choices3` (`id`, `question_number`, `is_correct`, `choice`) VALUES
(30, 1, 1, 'Caída de roca, caída a nivel, sobre esfuerzo, esquirlas en ojos, exposición a ruido, electrocución, exposición a polvo y líquidos presurizados.'),
(31, 1, 0, 'Caída de roca, caída a nivel, sobre esfuerzo'),
(32, 1, 0, 'Ninguno'),
(33, 1, 0, 'Exposición a polvo y líquidos presurizados.'),

(34, 2, 0, 'Cinto con auto rescatador, sordinas, casco.'),
(35, 2, 0, 'Cinto sin auto rescatador, sin sordinas, casco, guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),
(36, 2, 1, 'Cinto con auto rescatador, sordinas, casco, guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),
(37, 2, 0, 'Cinto con guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),

(38, 3, 0, 'Al finalizar de turno, se deberán revisar niveles de aceite y diésel, sistema supresor de incendios, extintor portátil, fugas, luces y torreta, así como, golpes y daños en el equipo'),
(39, 3, 0, 'Al inicio de turno, se deberán revisar niveles de diésel, sistema supresor de incendios, extintor portátil, fugas, luces y torreta'),
(40, 3, 1, 'Al inicio de turno, se deberán revisar niveles de aceite y diésel, sistema supresor de incendios, extintor portátil, fugas, luces y torreta, así como, golpes y daños en el equipo'),
(41, 3, 0, 'Al inicio de turno, no se deberán revisar niveles de aceite y diésel, sistema supresor de incendios, extintor portátil, fugas, luces y torreta'),

(42, 4, 1, 'Se llevará con el motor diésel encendido, el ayudante del operador va por delante para avisar que el equipo está en transito.'),
(43, 4, 0, 'Se llevará con el motor encendido, el ayudante del operador va por detras para avisar que el equipo está en transito.'),
(44, 4, 0, 'Se llevará con el motor diésel apagado'),
(45, 4, 0, 'Se llevará con el motor diésel apagado, el ayudante del operador va por delante para avisar que el equipo está en transito.'),

(46, 5, 0, 'Con el motor diésel encendido, se aleja del área de barrenación'),
(47, 5, 0, 'Con el motor encendido'),
(48, 5, 1, 'Con el motor diésel encendido, se acerca al área de barrenación'),
(49, 5, 0, 'Con el motor diésel apagado, se acerca al área de barrenación'),

(50, 6, 0, 'Condiciones de amacice'),
(51, 6, 0, 'Nicho y centinela, líneas de vida y arneses.'),
(52, 6, 1, 'Condiciones de amacice, ventilación, nicho y centinela, líneas de vida y arneses.'),
(53, 6, 0, 'Nada'),

(54, 7, 1, 'Sin tocar, se debe revisar primero el cuadro eléctrico centinela con un angelito, es muy importante revisar la estructura del simba para ver si no está energizada. '),
(55, 7, 0, 'Sin tocar, se debe revisar por último el cuadro eléctrico centinela con un angelito, no es importante revisar la estructura del simba '),
(56, 7, 0, 'Con un angelito'),
(57, 7, 0, 'Tocando, se debe revisar primero el cuadro eléctrico centinela con un angelito, es muy importante revisar la estructura del simba para ver si no está energizada. '),
--
(58, 8, 0, ' Bajar de inmediato el interruptor del centinela y dar aviso a personal de mantenimiento eléctrico.'),
(59, 8, 0, 'Nada y continuar con el trabajo'),
(60, 8, 0, ' Avisar al perosnal de mantenimiento eléctrico'),
(61, 8, 1, ' Bajar el interruptor'),

(62, 9, 0, 'Prevenir a los transeuntes de la actividad que se no está realizando'),
(63, 9, 1, 'Prevenir a los compañeros de la actividad que se está realizando'),
(64, 9, 0, 'Ninguna'),
(65, 9, 0, 'No prevenir a los compañeros de la actividad que se está realizando'),

(66, 10, 0, 'Nada'),
(67, 10, 0, 'Avisar por radio'),
(68, 10, 0, 'Dejar de trabajar'),
(69, 10, 1, 'Colocar otra lona o señalización en el límite del talud.');
