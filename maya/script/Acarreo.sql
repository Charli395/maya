INSERT INTO `questions7` (`question_number`, `question`) VALUES
(1, '¿Qué es Acarreo?'),
(2, '¿Qué es el pueble?'),
(3, '¿Qué es un derrame menor?'),
(4, '¿Qués es Tepetate?'),
(5, '¿A qué velocidad se deben transitar los vehículos en interior mina?'),
(6, 'Al encontrarse dos o más vehículos,¿quién debe realizar las maneobras necesarias para liberar el carril?'),
(7, '¿Dónde se debe realizar el paro de un vehículo?'),
(8, '¿Cómo se anuncia que se pondrña en movimiento un vehículo de acarreo?'),
(9, '¿En que consiste el Pre-arranque?'),
(10, '¿Que debe hacer el conductor de un vehículo al llegar a un cruce?');

INSERT INTO `choices7` (`id`, `question_number`, `is_correct`, `choice`) VALUES
(30, 1, 1, 'Mover de un lugar a otro el material ya sea mineral o tepetate'),
(31, 1, 0, 'Acumular el material en una zona'),
(32, 1, 0, 'Limpiar el área de trabajo'),
(33, 1, 0, 'Tumbar roca'),

(34, 2, 0, 'Ciudad'),
(35, 2, 0, 'Máquina'),
(36, 2, 1, 'Lugar en superficie donde se le dan indicaciones a los trabajadores'),
(37, 2, 0, 'Herramienta'),

(38, 3, 0, 'Un derrame de 80 cm de ancho'),
(39, 3, 0, 'Cualquier derrame'),
(40, 3, 1, 'Cualquier derrame de diesel o aceite de no más de 50 cm de ancho'),
(41, 3, 0, 'Ninguna'),

(42, 4, 1, 'Roca o material de relleno, sin valor ecónomico.'),
(43, 4, 0, 'Mineral con un alto valor económico'),
(44, 4, 0, 'Comida'),
(45, 4, 0, 'Lugar'),

(46, 5, 0, '60 km/h'),
(47, 5, 0, '70 km/h'),
(48, 5, 1, '20 km/h'),
(49, 5, 0, '90 km/h'),

(50, 6, 0, 'Cualquiera'),
(51, 6, 0, 'Ell que se encuentre más lejos de un cruce'),
(52, 6, 1, 'El que se encuentre más cerca de un cruce'),
(53, 6, 0, 'Ninguno'),

(54, 7, 1, 'En un lugar plano'),
(55, 7, 0, 'En un lugar inclinado'),
(56, 7, 0, 'Fuera de interior mina'),
(57, 7, 0, 'No sé'),

(58, 8, 0, 'No se avisa'),
(59, 8, 0, 'Por radio'),
(60, 8, 0, 'Gritando'),
(61, 8, 1, 'Con el claxón'),

(62, 9, 0, 'En encende rel vehículo'),
(63, 9, 1, 'Presentarse en el pueble, revisar checklist del vehículo'),
(64, 9, 0, 'Pasar por el pueble'),
(65, 9, 0, 'checklist'),

(66, 10, 0, 'Detenerse.'),
(67, 10, 0, 'Nada'),
(68, 10, 0, 'Seguir avanzando'),
(69, 10, 1, 'Parar el vehículo, asegurarse que no haya personas o equipo en movimiento');
