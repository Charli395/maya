INSERT INTO `questions` (`question_number`, `question`) VALUES
(1, '¿Qué es un Arficio?'),
(2, '¿Qué es un Bombillo'),
(3, '¿Qué es una Cañuela?'),
(4, '¿Qué es un Alto explosivo?'),
(5, '¿Qué es un cordón detonante?'),
(6, '¿Qués es un nonel?'),
(7, '¿Quienes pueden autorizar los vales de consumo de explosivos?'),
(8, '¿Cuáles son los elementos de seguridad que deben tener los vehiculos que transportan explosivos?'),
(9, '¿Cuánto es la cantidad máxima de transporte manual de explosivos'),
(10, 'Ejemplos de materiales anti chispa');

INSERT INTO `choices` (`id`, `question_number`, `is_correct`, `choice`) VALUES
(30, 1, 1, 'Material explosivo utilizado como iniciador de las detonaciones'),
(31, 1, 0, 'Material No explosivo'),
(32, 1, 0, 'No es el niciador de las detonaciones'),
(33, 1, 0, 'No sé'),

(34, 2, 0, 'No es un explosivo'),
(35, 2, 0, 'Herramienta'),
(36, 2, 1, 'Explosivo en forma de salchicha'),
(37, 2, 0, 'Nada'),

(38, 3, 0, 'No sé'),
(39, 3, 0, 'Cuerda no flexible con un interior de cordón de algodón '),
(40, 3, 1, 'Cuerda flexible con un interior de cordón de algodón hueco lleno de pólvora'),
(41, 3, 0, 'Cuerda hueca'),

(42, 4, 1, 'Material explosivo que produce la detonación masiva'),
(43, 4, 0, 'Material de plástico para rellenar huecos'),
(44, 4, 0, 'Material tóxico'),
(45, 4, 0, 'Nada'),

(46, 5, 0, 'Cable '),
(47, 5, 0, 'Cordón de algodón'),
(48, 5, 1, 'Cordón flexible con un núcleo de alto explosivo que se utiliza para iniciar otros explosivos'),
(49, 5, 0, 'Herramienta'),

(50, 6, 0, 'Detonante electrico'),
(51, 6, 0, 'Detonante'),
(52, 6, 1, 'Detonante no electrico, se puede utilizar tanto en tumbe en desarrollo y en producción'),
(53, 6, 0, 'No sé'),

(54, 7, 1, 'Facilitador, capitán de mina, asistente de mina y super intendente de mina'),
(55, 7, 0, 'Nadie'),
(56, 7, 0, 'Capitán de mina y yo'),
(57, 7, 0, 'Facilitador'),

(58, 8, 0, 'Ninguno'),
(59, 8, 0, 'Caja y piso forrados'),
(60, 8, 0, 'Banderolas, extintor, letrerosd¡ de peligro'),
(61, 8, 1, 'Banderolas, extintor, luces de emergencia, torretas, letreros de peligro, caja y piso forrados de materiales antichispa, compratimiento para los iniciadores de explosivos'),

(62, 9, 0, '10 kg'),
(63, 9, 1, '35 kg'),
(64, 9, 0, '90 kg'),
(65, 9, 0, '60 kg'),

(66, 10, 0, 'Ninguno'),
(67, 10, 0, 'Cualquier material'),
(68, 10, 0, 'Metales'),
(69, 10, 1, 'Madera, Aluminio, Cobre');
