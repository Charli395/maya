INSERT INTO `questions2` (`question_number`, `question`) VALUES
(1, '¿Cuáles son los peligros y riesgos?'),
(2, '¿Cuál es el equipo de protección personal?'),
(3, '¿Por qué es importante la tarjeta de pueble?'),
(4, 'Antes de iniciar la actividad, ¿qué se debe hacer?'),
(5, 'En caso de fugas de diésel o aceite, ¿a quién se debe informar?'),
(6, '¿Qué es importante recordar y aplicar?'),
(7, '¿Cuándo se coloca la lona o cinta de barricada?'),
(8, '¿Por qué es necesario reamacizar el área?'),
(9, '¿Cómo saber si el anclador está estabilizado y nivelado de forma correcta?'),
(10, 'Antes de iniciar actividades, ¿qué debe revisarse?');

INSERT INTO `choices2` (`id`, `question_number`, `is_correct`, `choice`) VALUES
(30, 1, 1, 'Caída de roca, caída a nivel, sobre esfuerzo, esquirlas en ojos, exposición a ruido, electrocución, exposición a polvo y líquidos presurizados.'),
(31, 1, 0, 'Caída de roca, caída a nivel, sobre esfuerzo'),
(32, 1, 0, 'Ninguno'),
(33, 1, 0, 'Exposición a polvo y líquidos presurizados.'),

(34, 2, 0, 'Cinto con auto rescatador, sordinas, casco.'),
(35, 2, 0, 'Cinto sin auto rescatador, sin sordinas, casco, guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),
(36, 2, 1, 'Cinto con auto rescatador, sordinas, casco, guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),
(37, 2, 0, 'Cinto con guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),

(38, 3, 0, 'No sé'),
(39, 3, 0, 'Porque tiene de no forma clara ni precisa la información de la actividad a realizar '),
(40, 3, 1, 'Porque tiene de forma clara y precisa la información de la actividad a realizar y los probables riesgos del lugar. En caso de ser necesario se hace un croquis del sitio.'),
(41, 3, 0, 'Porque así lo dicta el procedimiento'),

(42, 4, 1, 'Al inicio del turno, dar mantenimiento básico al jumbo, revisar aceites, sistema supresor de incendios y extintor portátil, presión de llantas, además de una revisión general del equipo, luces de traslado y frenos.  '),
(43, 4, 0, 'Una revisión general del equipo'),
(44, 4, 0, 'Al finalizar el turno, dar mantenimiento básico al jumbo, no revisar aceites o el sistema supresor de incendios y extintor portátil, presión de llantas'),
(45, 4, 0, 'Nada'),

(46, 5, 0, 'A los compañeros por radio'),
(47, 5, 0, 'A nadie'),
(48, 5, 1, 'A mantenimiento o al facilitador.'),
(49, 5, 0, 'A mi compañero'),

(50, 6, 0, 'Ciclo de observación STOP'),
(51, 6, 0, 'las cuatro A'),
(52, 6, 1, 'Ciclo de observación STOP y las cuatro A.'),
(53, 6, 0, 'Nada'),

(54, 7, 1, 'Al llegar al área de trabajo.'),
(55, 7, 0, 'Al retirarse del área de trabajo'),
(56, 7, 0, 'Nunca'),
(57, 7, 0, 'A veces'),

(58, 8, 0, 'Para continuar trabajando'),
(59, 8, 0, 'Porque me lo ordenan'),
(60, 8, 0, 'No sé'),
(61, 8, 1, 'Para asegurarnos que esté en las mejores condiciones para trabajar.'),

(62, 9, 0, 'Al menos una llanta debe estar suspendida'),
(63, 9, 1, 'Todas las llantas deben quedar suspendidas'),
(64, 9, 0, 'Ninguna llanta debe estar suspendida'),
(65, 9, 0, 'No sé'),

(66, 10, 0, 'Nada'),
(67, 10, 0, 'Que no haya nadie'),
(68, 10, 0, 'nivelar el anclador'),
(69, 10, 1, 'Primero, que no haya nadie cerca, después el operador debe estabilizar y nivelar el anclador.');
