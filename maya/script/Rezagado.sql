INSERT INTO `questions6` (`question_number`, `question`) VALUES
(1, 'Elige la opción con los significados correctos para las señales de seguridad'),
(2, '¿Cuál es el equipo de protección personal?'),
(3, '¿Cómo se debe trasladar el equipo al área de trabajo?'),
(4, '¿Qué se debe hacer antes de iniciar la actividad?'),
(5, '¿Cada cuantos cucharones se debe bajar el cucharon para limpieza de caminos?'),
(6, '¿De que lado debe camianr el personal que está en el área de trabajo?'),
(7, '¿Qué hacer cuando hay gente circulando en el área de trabajo?'),
(8, '¿Cómo debe realizarse el reamazise?'),
(9, '¿En que velocidad debe ir el vehículo cuando se mueve de reversa?'),
(10, '¿Qué hacer cuando no hay rezagado?');

INSERT INTO `choices6` (`id`, `question_number`, `is_correct`, `choice`) VALUES
(30, 1, 1, 'Luz de forma vertical=Retroceder, Luz de forma horizontal=Detente, Luz en círculos=Avanzar'),
(31, 1, 0, 'Luz de forma vertical=Alto, Luz de forma horizontal=Avanza, Luz en círculos= Retroceder'),
(32, 1, 0, 'Luz de forma vertical=Avanzar, Luz de forma horizontal=Retroceder, Luz en círculos=Detente'),
(33, 1, 0, 'Exposición a polvo y líquidos presurizados.'),

(34, 2, 0, 'Cinto con auto rescatador, sordinas, casco.'),
(35, 2, 0, 'Cinto sin auto rescatador, sin sordinas, casco, guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),
(36, 2, 1, 'Cinto con auto rescatador, sordinas, casco, guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),
(37, 2, 0, 'Cinto con guantes, lentes, respirador, ropa con manga larga y reflejantes y botas de hule.'),

(38, 3, 0, 'No sé'),
(39, 3, 0, 'A más de 20 km/h,cucharon donde se tenga visibilidad, niveles de rampas y motor por detrás,luces frontales encendidas'),
(40, 3, 1, 'A no más de 20 km/h,cucharon donde se tenga visibilidad, niveles de rampas y motor por detrás,luces frontales encendidas'),
(41, 3, 0, 'Motor por detrás,luces frontales encendidas'),

(42, 4, 1, 'Verificar la ventilación del lugar, detector de CO2, Verificar la línea de rezago '),
(43, 4, 0, 'Una revisión general del equipo'),
(44, 4, 0, 'Al finalizar el turno, dar mantenimiento básico al equipo, amacizar'),
(45, 4, 0, 'Nada'),

(46, 5, 0, '5'),
(47, 5, 0, '1'),
(48, 5, 1, '3'),
(49, 5, 0, '7'),

(50, 6, 0, 'Frente al equipo'),
(51, 6, 0, 'Por detrás del equipo'),
(52, 6, 1, 'Del lado de la cabina'),
(53, 6, 0, 'Contrario a la cabina'),

(54, 7, 1, 'Detenerse y poner el cucharon hacia el piso'),
(55, 7, 0, 'Nada'),
(56, 7, 0, 'Seguir trabajando normalmente'),
(57, 7, 0, 'Detenerse'),

(58, 8, 0, 'Nunca'),
(59, 8, 0, 'Solo'),
(60, 8, 0, 'En grupos de 10 personas'),
(61, 8, 1, 'En cuadrillas'),

(62, 9, 0, '1a velocidad'),
(63, 9, 1, '2da velocidad'),
(64, 9, 0, '3a velocidad'),
(65, 9, 0, 'No sé'),

(66, 10, 0, 'Nada'),
(67, 10, 0, 'Avisar por radio, esperar al supervisor'),
(68, 10, 0, 'Verificar el equipo.'),
(69, 10, 1, 'Utilizar las señales de seguridad');
