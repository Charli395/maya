<?php
session_start();

?>

<!doctype html>
<html lang="en">
  <head>
    <title>Creando Sesion</title>
	
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="fonts.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
   <header class="contenedor">
          <img src="img/fresnillo.png" align="center">            
  </header>
  
  <body class="contenedor">  
 	<div class="login">
 	<article>
  
<?php

	// Connection info. file
	include 'conn.php';	
	
	// Connection variables
	$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

	// Check connection
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	
	// data sent from form login.html 
	$usuario = $_POST['usuario']; 
	$password = $_POST['password'];
	
	// Query sent to database
	$result = mysqli_query($conn, "SELECT usuario, password,nombre, puesto, unidad, contratista FROM usuario WHERE usuario = '$usuario'");
	
	// Variable $row hold the result of the query
	$row = mysqli_fetch_assoc($result);
	
	// Variable $hash hold the password hash on database
	$hash = $row['password'];
	
	/* 
	password_Verify() function verify if the password entered by the user
	match the password hash on the database. If everything is ok the session
	is created for one minute. Change 1 on $_SESSION[start] to 5 for a 5 minutes session.
	*/
	if (password_verify($_POST['password'], $hash)) {	
		
		$_SESSION['loggedin'] = true;
		$_SESSION['usuario'] = $row['usuario'];
		$_SESSION['nombre'] = $row['nombre'];
		$_SESSION['unidad'] = $row['unidad'];
		$_SESSION['contratista'] = $row['contratista'];
		$_SESSION['puesto'] = $row['puesto'];
		$_SESSION['start'] = time();
		$_SESSION['expire'] = $_SESSION['start'] + (1 * 60) ;						
		#header("Location: bienvenido.php");
		/*echo "<div class='alert alert-success' role='alert'><strong>Welcome!$row[usuario]	</strong> 		
		<p><a href='edit-profile.php'>Edit Profile</a></p>	
		<p><a href='logout.php'>Logout</a></p></div>";	*/
		echo '<script type="text/javascript">location.href="bienvenido.php"</script>';

	
	} else {

			echo "<div class='alert alert-danger' role='alert'>¡Usuario o contraseña son incorrectos!
		<p><a href='index.html'><strong>Intentar Nuevamente!</strong></a></p></div>";		
		
	}	
?>
</article>
</div>

	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

	</body>

	<footer align="center" class="contenedor">
      <p> © 2019 Maya producciones. Todos los derechos reservados</p>
  </footer>
</html>