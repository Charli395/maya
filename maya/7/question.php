<?php include "database.php"; ?>
<?php session_start(); ?>
<?php
	//Set question number
	$number = (int) $_GET['n'];

	//Get total number of questions
	$query = "select * from questions6";
	$results = $mysqli->query($query) or die($mysqli->error.__LINE__);
	$total=$results->num_rows;

	// Get Question
	$query = "select * from `questions6` where question_number = $number";

	//Get result
	$result = $mysqli->query($query) or die($mysqli->error.__LINE__);
	$question = $result->fetch_assoc();


	// Get Choices
	$query = "select * from `choices6` where question_number = $number";

	//Get results
	$choices = $mysqli->query($query) or die($mysqli->error.__LINE__);

 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Carga de explosivos</title>
    <link rel="stylesheet" href="css/style.css" type="text/css" />
     <meta charset="utf-8">
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../estilos.css">
    <link rel="stylesheet" href="../fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Montez|Pathway+Gothic+One" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <style>
.btn {
  background-color: red;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;

}

/* Darker background on mouse-over */
.btn:hover {
  background-color: #ee3a1f;
}
#myProgress {
  position: relative;
  width: 100%;
  height: 30px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 10%;
  height: 100%;
  background-color: red;
}

#label {
  text-align: center;
  line-height: 30px;
  color: white;
}
</style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css">
 
  </head>
<header class="contenedor"> 
  <div class="container">
  <div class="row"> 
    <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="row">
      <div class="form-group">        
        <img src="../img/user.png" alt="User" width="90px" height="90px">
      </div>
      <div class="form-group">        
        <?php
                if (isset($_SESSION['nombre'],$_SESSION['puesto'])) {

                  echo "
                  <br>
                    <div><strong><p style='color:black' align='center'>$_SESSION[nombre]</p></strong>
                    <p style='color:black' align='top' >$_SESSION[puesto]/$_SESSION[unidad]/$_SESSION[contratista]</p>
          </div>
                   

                  ";
                }else{
                    echo '<script type="text/javascript">location.href="../index.html"</script>';
                }
              ?>
      </div>
    </div><!-- termina primer row del row-->    
      
    </div><!-- termina primer columna-->
      
    <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">        
        <img src="../img/fresnillo.png" align="right">
      </div>
    </div>
  </div>
</div>
</header>

<script type="text/javascript">
	
	function nobackbutton(){
	
   window.location.hash="no-back-button";
	
   window.location.hash="Again-No-back-button" //chrome
	
   window.onhashchange=function(){window.location.hash="no-back-button";}
	
}
</script>
  <script type="text/javascript">

    var cronometro;
    function detenerse()

    {

        clearInterval(cronometro);

    } // se termina funcion detenerse 

    function carga()

    {

        contador_s =0;

        contador_m =0;

        s = document.getElementById("segundos");

        m = document.getElementById("minutos");

        cronometro = setInterval(

            function(){

                if(contador_s==60){

                    contador_s=0;

                    contador_m++;

                    m.innerHTML = contador_m;

                    if(contador_m==60)

                    {

                        contador_m=0;

                    }
                }// termina el primer if

              s.innerHTML = contador_s;

                contador_s++;
            }

            ,1000);

    }//termina funcion carga 
   </script>
  <body class="otro" onload="nobackbutton();carga();">
  	<main>
  		 <div class="container">
		    <div class="progress">
		      <div class="progress-bar progress-bar-success" role="progressbar" style="width:75%;background-color: #ee3a1f">
		     75%
		      </div>
		    </div>
		</div>
		<br>
      <div class="container">
        <div align="right"><strong>Pregunta <?php echo $number; ?> de  <?php echo $total; ?></strong></div>
			<div class="current" style="background-color: #BBB9B9;border-top-left-radius: 15px;border-top-right-radius: 15px;">
				<strong>
					<p  style="background-color: #BBB9B9" >
			   <?php echo $question['question'] ?>
				</p>
				</strong>
			</div>
			
			<form method="post" action="process.php" >
			      <ul class="choices" style="background-color: #ffffff; border-bottom-right-radius: 10px;
  									border-bottom-left-radius: 10px;" align="left">
			        <?php while($row=$choices->fetch_assoc()): ?>
					<li><input name="choice" type="radio" value="<?php echo $row['id']; ?>" />
					  <?php echo $row['choice']; ?>
					</li>
					<?php endwhile; ?>
			      </ul>
			      
			      <div align="center">
			      	<input style="border-top-right-radius: 10px;background-color: red;border-top-left-radius: 10px;border-bottom-right-radius: 10px;color:white;border-bottom-left-radius: 10px;width:200px;height:40	px;"type="submit" value="Calificar" /> 
			      	<input type="hidden" name="number" value="<?php echo $number; ?>" />
			      </div>     
			</form>
      </div>
  </body>
  	</main>
  	 
  <br>
    <footer align="center" class="contenedor">
    	<strong>
    		<p style="color: red;">
		    	<span>Tiempo:</span>
		        <span id="minutos">0</span>:<span id="segundos">0</span>

    		</p>
    	</strong>

      <p> © 2019 Maya producciones. Todos los derechos reservados</p>
  </footer>
</html>